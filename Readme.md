# IFI : Orchestration docker

----------
> Binôme :
  - Pape Ibrahima BADIANE   
  - Mahamoud Mohamed FARAH    

----------
>***A faire ***:
> - CreateImage avec dockerFile
> - log4j
> 


## I - Controllers
#### Manager.java
Ce controller gère les réquetes suivantes :
* `/containers` : demande au service `ApiDocker.java` de générer la liste des conteneurs. Ensuite il transmet cette liste au template `containers.html`.

* `/images` : demande au service `ApiDocker.java` de générer la liste des images. Ensuite il transmet cette liste au template `images.html`.


## II - Services
#### ApiDocker.java
Cette class java s'appuie sur l'[api docker-java](https://github.com/docker-java/docker-java"). Elle établie donc une connexion, même à distante avec la machine hôte du docker. Pour ensuite executer des commandes docker. Cette classe est le pivot entre le controller et l'api docker-java.

## III - Templates
#### Containers.html
Cette page html génère affiche la liste des conteneurs docker.

##### Images.html
Cette page html génère affiche la liste des images docker.
