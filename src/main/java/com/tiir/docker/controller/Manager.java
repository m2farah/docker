package com.tiir.docker.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tiir.docker.service.ApiDocker;

@Controller
public class Manager {
	
	@Autowired
	ApiDocker apiDocker;
	
	
	@GetMapping("/")
	public String home() {
		return "index";
	}
	
//	@GetMapping("/containers")
//	public String listContainers() {
//		return apiDocker.findAllContainers();
//	}
	
	@GetMapping("/test")
	public String test() {
		return "test";
	}
	
	@GetMapping("/images")	
	public String ListImages(ModelMap model){
		model.addAttribute("dockerHost", apiDocker.getDockerHost());
		model.addAttribute("images", apiDocker.getAllImages());
		return "images";
	}
	
	@GetMapping("/containers")	
	public String ListContainers(ModelMap model){
		model.addAttribute("dockerHost", apiDocker.getDockerHost());
		model.addAttribute("containers", apiDocker.getAllContainers());
		return "containers";
	}
	
	@GetMapping("/containers/{containerId}/{status}")
	public String changeStatusContainer(@PathVariable String containerId, @PathVariable String status, ModelMap model) {
		if (status.equals("start"))
			apiDocker.startContainer(containerId);
		else if (status.equals("stop"))
			apiDocker.stopContainer(containerId);
		else if (status.equals("remove"))
			apiDocker.removeContainer(containerId);

		/* on recharge réaffiche la liste des conteneurs */
		return "redirect:/containers";
	}
	
	@GetMapping("/images/{imageId}/{status}")
	public String removeImage(@PathVariable String imageId, @PathVariable String status) {
		if (status.equals("remove"))
			apiDocker.removeImage(imageId);
		return "redirect:/images";
	}
	
	@GetMapping("/containers/create/{imageRepoName}")
	public String createContainerFromImage(@PathVariable String imageRepoName) {
		apiDocker.createContainerFromImage(imageRepoName);
		return "redirect:/containers";
	}
	
	@GetMapping("/images/pull/{repoName}")	
	public String pullImage(@PathVariable String repoName) throws Exception {
		apiDocker.pullImage(repoName);
		return "redirect:/images";
	}
	
	@GetMapping("/images/build")
	public ModelAndView buildImage() {
		return new ModelAndView("dockerFile");
	}
	
//	@PostMapping("/images/build")	
//	public void postBuildImageFromDockerFile(HttpServletRequest request, HttpServletResponse response) throws Exception {
//		System.out.println("Je suis là");
//		File dockerFile = new File(request.getParameter("dockerFile"));
//		apiDocker.buildImage(dockerFile);
//	}
	
	@PostMapping("/images/build")	
	public void postBuildImageFromDockerFile(@RequestParam("dockerFile") MultipartFile file) throws Exception {
		System.out.println("FileName:"+file.getName()+"   File");
		
		apiDocker.buildImage(new File(file.getName()));
	}

}
