package com.tiir.docker.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.exception.DockerClientException;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.BuildResponseItem;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.command.BuildImageResultCallback;
import com.github.dockerjava.core.command.PullImageResultCallback;

@Service
public class ApiDocker {

    
    private String dockerHost = "localhost";// dockerHost doit être de la forme de IP:PORT
    private DockerClientConfig config;
	private DockerClient docker;
	
	private final SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy à HH:mm");

	/**
	 * Initialise l'api docker avec l'hôte localhost.
	 */
	public ApiDocker(){
		this.config = DefaultDockerClientConfig.createDefaultConfigBuilder()
			    .withDockerHost("unix:///var/run/docker.sock")
			    .build();
		this.docker = DockerClientBuilder.getInstance(config).build();
	}
	
	/**
	 * Donne l'hôte contenant le docker que l'on gère actuellement.
	 * @return l'adresse de l'hôte contenant le docker que l'on gère
	 */
    public String getDockerHost() {
		return dockerHost;
	}

    /**
     * Change le docker que l'on veut gérer. 
     * @param dockerHost - doit être former de cette manière "IP:PORT". Et il répresente l'hôte du docker à gérer.
     */
	public void setDockerHost(String dockerHost) {
		this.dockerHost = dockerHost;
		this.config = DefaultDockerClientConfig.createDefaultConfigBuilder()
			    .withDockerHost("tcp://"+dockerHost)
			    .build();
		this.docker = DockerClientBuilder.getInstance(config).build();
	}

	/**
	 * Récupère la liste de conteneurs.
	 * @return liste de conteneur docker
	 */
	public List<Container> getAllContainers(){
    	return docker.listContainersCmd().withShowAll(true).exec();// withShowAll permet d'afficher tous même ceux qui ne sont pas running
    }
	
	/**
	 * Crée une liste d'images.
	 * @return liste d'images docker.
	 */
    public List<Image> getAllImages(){
    	return docker.listImagesCmd().exec();
    }
    
    
    public void startContainer(String containerId) {
    	if (docker.inspectContainerCmd(containerId).exec().getState().getStatus().equals("exited") || docker.inspectContainerCmd(containerId).exec().getState().getStatus().equals("created")) {
    		docker.startContainerCmd(containerId).exec();
    	}
    }
    
    public void stopContainer(String containerId) {
    	if (docker.inspectContainerCmd(containerId).exec().getState().getStatus().equals("running")) {
    		docker.stopContainerCmd(containerId).exec();
    	}
    }
    
    public void removeContainer(String containerId) {
    	if (docker.inspectContainerCmd(containerId).exec().getState().getStatus().equals("running")) {
    		docker.stopContainerCmd(containerId).exec();
    	}
    	docker.removeContainerCmd(containerId).exec();		
    }
    
    /**
     * Crée un container à partir d'une image déjà existante dans la liste d'images. Par défaut, le container est crée avec l'option "-d", e.g docker run -d debian
     * @param image - peut être soit le nom du répository, soit l'id de l'image 
     */
    public void createContainerFromImage(String image) {
    	docker.createContainerCmd(image).withStdinOpen(true).withTty(true).exec();
    }
    
    public void removeImage(String imageId) {
    	docker.removeImageCmd(imageId).exec();
    }
    
    /**
     * Pull une image. Et attendre jusqu'à ce que l'image soit complètement pull.
     * @param repoName - doit être de cette forme repositoryName:tag, i.e ubuntu:latest
     * @throws InterruptedException 
     */
    public void pullImage(String repoName) throws InterruptedException {
    	PullImageResultCallback callback = new PullImageResultCallback();
    	docker.pullImageCmd(repoName).exec(callback);
    	System.out.println("BuildImage");
    	callback.awaitCompletion();

    }
    
    
    /**
     * Crée une image docker à partir d'un dockerfile
     * @param dockerFile - répresente le dockerfile 
     * @return l'id de l'image
     */
    public String buildImage(File dockerFile) {
    	BuildImageResultCallback callback = new BuildImageResultCallback() {
		    @Override
		    public void onNext(BuildResponseItem item) {
		       System.out.println("" + item);
		       super.onNext(item);
		    }
		};

		return docker.buildImageCmd(dockerFile).exec(callback).awaitImageId();
    }

//    public String findAllContainers() throws RestClientException {
//        return this.restTemplate.getForObject("http://localhost:4444/containers/json?all=1", String.class);
//    }
}
